package sample;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.text.Font;

public  class EditEmployee extends AnchorPane  {

    protected final GridPane EmployeeDetailsGridPane;
    protected final ColumnConstraints columnConstraints;
    protected final ColumnConstraints columnConstraints0;
    protected final ColumnConstraints columnConstraints1;
    protected final RowConstraints rowConstraints;
    protected final Button updateButton;
    protected final Button cancelButton;
    protected final Button deleteButton;
    protected final Label nameLabel;
    protected final Label phoneLabel;
    protected final Label addressLabel;
    protected final Label emailLabel;
    protected final TextField nameTextField;
    protected final TextField phoneTextField;
    protected final TextField addressTextField;
    protected final TextField emailTextField;

    public EditEmployee() {

        EmployeeDetailsGridPane = new GridPane();
        columnConstraints = new ColumnConstraints();
        columnConstraints0 = new ColumnConstraints();
        columnConstraints1 = new ColumnConstraints();
        rowConstraints = new RowConstraints();
        updateButton = new Button();
        cancelButton = new Button();
        deleteButton = new Button();
        nameLabel = new Label();
        phoneLabel = new Label();
        addressLabel = new Label();
        emailLabel = new Label();
        nameTextField = new TextField();
        phoneTextField = new TextField();
        addressTextField = new TextField();
        emailTextField = new TextField();

        setMaxHeight(USE_PREF_SIZE);
        setMaxWidth(USE_PREF_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefHeight(400.0);
        setPrefWidth(600.0);

        EmployeeDetailsGridPane.setLayoutY(327.0);
        EmployeeDetailsGridPane.setPrefHeight(73.0);
        EmployeeDetailsGridPane.setPrefWidth(600.0);

        columnConstraints.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints.setMinWidth(10.0);
        columnConstraints.setPrefWidth(100.0);

        columnConstraints0.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints0.setMinWidth(10.0);
        columnConstraints0.setPrefWidth(100.0);

        columnConstraints1.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints1.setMinWidth(10.0);
        columnConstraints1.setPrefWidth(100.0);

        rowConstraints.setMinHeight(10.0);
        rowConstraints.setPrefHeight(30.0);
        rowConstraints.setVgrow(javafx.scene.layout.Priority.SOMETIMES);

        updateButton.setId("updateButton");
        updateButton.setMnemonicParsing(false);
        updateButton.setPrefHeight(98.0);
        updateButton.setPrefWidth(315.0);
        updateButton.setText("Update");

        GridPane.setColumnIndex(cancelButton, 2);
        cancelButton.setId("cancelButton");
        cancelButton.setMnemonicParsing(false);
        cancelButton.setPrefHeight(159.0);
        cancelButton.setPrefWidth(341.0);
        cancelButton.setText("Cancel");

        GridPane.setColumnIndex(deleteButton, 1);
        deleteButton.setId("deleteButton");
        deleteButton.setMnemonicParsing(false);
        deleteButton.setPrefHeight(222.0);
        deleteButton.setPrefWidth(303.0);
        deleteButton.setText("Delete");

        nameLabel.setId("nameLabel");
        nameLabel.setLayoutX(31.0);
        nameLabel.setLayoutY(14.0);
        nameLabel.setPrefHeight(57.0);
        nameLabel.setPrefWidth(95.0);
        nameLabel.setText("Name:");
        nameLabel.setFont(new Font("System Bold", 24.0));

        phoneLabel.setId("phoneLabel");
        phoneLabel.setLayoutX(31.0);
        phoneLabel.setLayoutY(78.0);
        phoneLabel.setPrefHeight(57.0);
        phoneLabel.setPrefWidth(95.0);
        phoneLabel.setText("Phone:");
        phoneLabel.setFont(new Font("System Bold", 24.0));

        addressLabel.setId("addressLabel");
        addressLabel.setLayoutX(31.0);
        addressLabel.setLayoutY(143.0);
        addressLabel.setPrefHeight(57.0);
        addressLabel.setPrefWidth(122.0);
        addressLabel.setText("Address:");
        addressLabel.setFont(new Font("System Bold", 24.0));

        emailLabel.setId("emailLabel");
        emailLabel.setLayoutX(31.0);
        emailLabel.setLayoutY(216.0);
        emailLabel.setPrefHeight(57.0);
        emailLabel.setPrefWidth(95.0);
        emailLabel.setText("Email:");
        emailLabel.setFont(new Font("System Bold", 24.0));

        nameTextField.setLayoutX(228.0);
        nameTextField.setLayoutY(30.0);
        nameTextField.setPrefHeight(33.0);
        nameTextField.setPrefWidth(310.0);

        phoneTextField.setLayoutX(228.0);
        phoneTextField.setLayoutY(94.0);
        phoneTextField.setPrefHeight(33.0);
        phoneTextField.setPrefWidth(310.0);

        addressTextField.setLayoutX(228.0);
        addressTextField.setLayoutY(152.0);
        addressTextField.setPrefHeight(33.0);
        addressTextField.setPrefWidth(310.0);

        emailTextField.setLayoutX(228.0);
        emailTextField.setLayoutY(225.0);
        emailTextField.setPrefHeight(33.0);
        emailTextField.setPrefWidth(310.0);

        EmployeeDetailsGridPane.getColumnConstraints().add(columnConstraints);
        EmployeeDetailsGridPane.getColumnConstraints().add(columnConstraints0);
        EmployeeDetailsGridPane.getColumnConstraints().add(columnConstraints1);
        EmployeeDetailsGridPane.getRowConstraints().add(rowConstraints);
        EmployeeDetailsGridPane.getChildren().add(updateButton);
        EmployeeDetailsGridPane.getChildren().add(cancelButton);
        EmployeeDetailsGridPane.getChildren().add(deleteButton);
        getChildren().add(EmployeeDetailsGridPane);
        getChildren().add(nameLabel);
        getChildren().add(phoneLabel);
        getChildren().add(addressLabel);
        getChildren().add(emailLabel);
        getChildren().add(nameTextField);
        getChildren().add(phoneTextField);
        getChildren().add(addressTextField);
        getChildren().add(emailTextField);

    }
}
