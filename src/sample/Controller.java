package sample;


import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;
import java.util.*;

import javax.print.Doc;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class Controller {

    private XmlCrudBase xmlCrudBase;
    private File selectedFile;
    private NewEmployee newEmployee;
    private EditEmployee editEmployee;
    private EmployeeDetails employeeDetails;
    private List<Employee> employeeList;
    private ListIterator<Employee> iterator ;
    private int employeeIndex;



    public Controller(XmlCrudBase xmlCrudBase) {
        this.xmlCrudBase = xmlCrudBase;
        this.employeeIndex = 0;
        setXmlCrudBaseActions();

    }

    private void setXmlCrudBaseActions(){
        this.xmlCrudBase.openMenuItem.setOnAction(event -> openEmployeeXmlFile());
        this.xmlCrudBase.showMenuItem.setOnAction(event -> employeeDetailsPane());
        this.xmlCrudBase.newMenuItem.setOnAction(event -> newEmployeePane());
        this.xmlCrudBase.editMenuItem.setOnAction(event -> editEmployeePane());
        this.xmlCrudBase.closeMenuItem.setOnAction(event -> System.exit(0));
    }




    //New
    private void newEmployeePane(){
        System.out.println(this.selectedFile.toString());
        this.newEmployee  = new NewEmployee();
        this.xmlCrudBase.setCenter(newEmployee);
        newEmployee.createButton.setOnAction(event -> createNewEmployee());
        newEmployee.cancelButton.setOnAction(event -> clearTextFields(this.newEmployee));
    }

    private void createNewEmployee(){

        if(this.newEmployee.nameTextField.getText().isEmpty() || this.newEmployee.phoneTextField.getText().isEmpty()
                || this.newEmployee.addressTextField.getText().isEmpty() ||
                this.newEmployee.emailTextField.getText().isEmpty()
        ){
            emptyInputAlert();
        }else if(!validateEmail(this.newEmployee.emailTextField.getText())){
            nonValidEmailAlert();

        }else{
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder;
            try {
                documentBuilder = documentBuilderFactory.newDocumentBuilder();
                Document doc = documentBuilder.parse(this.selectedFile);
                Element rootElement = doc.getDocumentElement();
                rootElement.appendChild(appendEmployeeNodes(
                        doc,
                        this.newEmployee.nameTextField.getText(),
                        this.newEmployee.phoneTextField.getText(),
                        this.newEmployee.addressTextField.getText(),
                        this.newEmployee.emailTextField.getText()
                ));
                saveChangesToXmlFile(doc);
                updateEmployeeList(doc);
                creationAlert();
            }catch (ParserConfigurationException | SAXException | IOException e) {
                e.printStackTrace();
            }
        }


    }

    private Node appendEmployeeNodes(Document doc,String name,String phone, String address, String email){
        Element employee = doc.createElement("Employee");
        employee.appendChild(createEmployeeNodeData(doc, employee, "Name", name));
        employee.appendChild(createEmployeeNodeData(doc, employee, "Phone", phone));
        employee.appendChild(createEmployeeNodeData(doc, employee, "Address", address));
        employee.appendChild(createEmployeeNodeData(doc, employee, "Email", email));
        return employee;
    }

    private Node createEmployeeNodeData(Document doc, Element element,String tagName, String value){
        Element node = doc.createElement(tagName);
        node.appendChild(doc.createTextNode(value));
        return node;
    }

    private void clearTextFields(NewEmployee newEmployee){
        newEmployee.nameTextField.setText("");
        newEmployee.phoneTextField.setText("");
        newEmployee.addressTextField.setText("");
        newEmployee.emailTextField.setText("");
    }

    //Open
    //Open Employee Xml File With File Chooser
    // convert each node into employee object
    // put them in arrayList
    private void openEmployeeXmlFile(){
        FileChooser fileChooser = new FileChooser();
        this.selectedFile = fileChooser.showOpenDialog(null);
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder;
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document doc = documentBuilder.parse(this.selectedFile);
            doc.getDocumentElement().normalize();
            updateEmployeeList(doc);
        } catch (ParserConfigurationException |SAXException| IOException  e) {
            e.printStackTrace();
        }
    }

    private void updateEmployeeList(Document doc){
        NodeList nodeList = doc.getElementsByTagName("Employee");
        this.employeeList = new ArrayList<>();
        for(int i=0; i< nodeList.getLength(); i++){
            this.employeeList.add(getEmployee(nodeList.item(i)));
        }
        this.iterator = this.employeeList.listIterator();
    }

    //create employee object with node data from xml file
    private Employee getEmployee(Node node){
        Employee employee = new Employee();
        if(node.getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element) node;
            employee.setName(getTagValue("Name",element));
            employee.setAddress(getTagValue("Address",element));
            employee.setPhone(getTagValue("Phone",element));
            employee.setEmail(getTagValue("Email",element));
        }
        return employee;
    }

    // return data from node list and return value with tagName
    private String getTagValue(String tag, Element element) {

        Node node = element.getElementsByTagName(tag).item(0).getFirstChild();
        return node.getNodeValue();
    }
    // set employeeDetailsPane
    private void employeeDetailsPane(){
        this.employeeDetails = new EmployeeDetails();
        firstPageData();
        this.xmlCrudBase.setCenter(employeeDetails);
        this.employeeDetails.nextButton.setOnAction(event -> nextData());
        this.employeeDetails.previousButton.setOnAction(event -> previousData());
        this.employeeDetails.editButton.setOnAction(event -> editEmployeePane());
        this.employeeDetails.deleteButton.setOnAction(event -> deleteEmployee());
    }

    private void firstPageData(){
        this.employeeDetails.nameTextArea.setText(this.employeeList.get(0).getName());
        this.employeeDetails.phoneTextArea.setText(this.employeeList.get(0).getPhone());
        this.employeeDetails.addressTextArea.setText(this.employeeList.get(0).getAddress());
        this.employeeDetails.emailTextArea.setText(this.employeeList.get(0).getEmail());
    }

    //nextButton Get next Employee object from employees list
    private void nextData(){
        if (this.iterator.hasNext()){
            this.employeeIndex = iterator.nextIndex();
            Employee employee = iterator.next();
            this.employeeDetails.nameTextArea.setText(employee.getName());
            this.employeeDetails.phoneTextArea.setText(employee.getPhone());
            this.employeeDetails.addressTextArea.setText(employee.getAddress());
            this.employeeDetails.emailTextArea.setText(employee.getEmail());
        }else {
            nextAlert();

        }
    }

    // Get previous Employee object from employees list
    private void previousData(){
        if (this.iterator.hasPrevious()){
            this.employeeIndex = iterator.previousIndex();
            Employee employee = iterator.previous();
            this.employeeDetails.nameTextArea.setText(employee.getName());
            this.employeeDetails.phoneTextArea.setText(employee.getPhone());
            this.employeeDetails.addressTextArea.setText(employee.getAddress());
            this.employeeDetails.emailTextArea.setText(employee.getEmail());
        }else {
            previousAlert();
        }

    }

    //Edit
    private void editEmployeePane(){
        this.editEmployee = new EditEmployee();
        this.xmlCrudBase.setCenter(editEmployee);
        this.editEmployee.nameTextField.setText(this.employeeList.get(this.employeeIndex).getName());
        this.editEmployee.phoneTextField.setText(this.employeeList.get(this.employeeIndex).getPhone());
        this.editEmployee.addressTextField.setText(this.employeeList.get(this.employeeIndex).getAddress());
        this.editEmployee.emailTextField.setText(this.employeeList.get(this.employeeIndex).getEmail());
        this.editEmployee.updateButton.setOnAction(event -> editEmployeeData());
        this.editEmployee.cancelButton.setOnAction(event -> employeeDetailsPane());
        this.editEmployee.deleteButton.setOnAction(event -> deleteEmployee());
    }

    private void editEmployeeData(){

        if(this.editEmployee.nameTextField.getText().isEmpty() || this.editEmployee.phoneTextField.getText().isEmpty()
                || this.editEmployee.addressTextField.getText().isEmpty() ||
                this.editEmployee.emailTextField.getText().isEmpty()
        ){
            emptyInputAlert();
        }else if(!validateEmail(this.editEmployee.emailTextField.getText())){
            nonValidEmailAlert();

        }else{
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder;
            try {
                documentBuilder = documentBuilderFactory.newDocumentBuilder();
                Document doc = documentBuilder.parse(this.selectedFile);
                doc.getDocumentElement().normalize();
                NodeList empNodeList = doc.getElementsByTagName("Employee");
                Element emp = (Element) empNodeList.item(this.employeeIndex);
                editEmployeeNodes(doc,emp,"Name",this.editEmployee.nameTextField.getText());
                editEmployeeNodes(doc,emp,"Phone",this.editEmployee.phoneTextField.getText());
                editEmployeeNodes(doc,emp,"Address",this.editEmployee.addressTextField.getText());
                editEmployeeNodes(doc,emp,"Email",this.editEmployee.emailTextField.getText());
                saveChangesToXmlFile(doc);
                updateEmployeeList(doc);
                editAlert();
            } catch (ParserConfigurationException | SAXException | IOException e) {
                e.printStackTrace();
            }
        }



    }

    private void editEmployeeNodes(Document doc,Element element ,String tagName, String value){
        Node node = element.getElementsByTagName(tagName).item(0).getFirstChild();
        node.setNodeValue(value);

    }


    private void saveChangesToXmlFile(Document doc){
        DOMSource source = new DOMSource(doc);
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = null;
        try {
            transformer = transformerFactory.newTransformer();
            StreamResult result = new StreamResult(this.selectedFile);
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(source,result);
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        updateEmployeeList(doc);
    }




    //Delete
    private void deleteEmployee(){
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder;
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document doc = documentBuilder.parse(this.selectedFile);
            doc.getDocumentElement().normalize();
            NodeList empNodeList = doc.getElementsByTagName("Employee");
            Element emp = (Element) empNodeList.item(this.employeeIndex);
            emp.getParentNode().removeChild(emp);
            this.employeeList.remove(this.employeeIndex);
            saveChangesToXmlFile(doc);
            updateEmployeeList(doc);
            deleteAlert();
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }



    //creation alert
    private void creationAlert(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog with Custom Actions");
        alert.setHeaderText("Look, a Confirmation Dialog with Custom Actions");
        alert.setContentText("Choose your option.");
        ButtonType buttonTypeOne = new ButtonType("Show Employees");
        ButtonType buttonTypeTwo = new ButtonType("Exit");

        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo);

        Optional<ButtonType> choice = alert.showAndWait();
        if (choice.get() == buttonTypeOne){
            employeeDetailsPane();
        }
        if (choice.get() == buttonTypeTwo) {
            System.exit(0);
        }
    }

    /***************************  Validations *********************/

    private boolean validateEmail(String email){
        return email.matches("[a-zA-Z0-9._%+-][a-zA-Z0-9._%+-][a-zA-Z0-9._%+-]+@[a-zA-Z]+\\.[a-zA-Z]{3}");
    }



    private boolean validateEmptyInput(String userInput){
        if(userInput.isEmpty()){
            return true;
        }else{
            return false;
        }
    }


    /************************ Alert ******************************/
    /* NextAlert */
    private void nextAlert(){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning Dialog");
        alert.setHeaderText("No Other next Elements");
        alert.setContentText("you have reached the last Employee");
        alert.showAndWait();
    }
    /* PreviousAlert */
    private void previousAlert(){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning Dialog");
        alert.setHeaderText("No Other previous Elements");
        alert.setContentText("you have reached the first Employee");
        alert.showAndWait();
    }
    /* edit Alert */

    private void editAlert(){

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Edit Success Message");
        alert.setHeaderText("Employee has been Updated");
        alert.setContentText("Choose your option.");
        ButtonType buttonTypeOne = new ButtonType("Show Employees");
        ButtonType buttonTypeTwo = new ButtonType("Exit");

        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo);

        Optional<ButtonType> choice = alert.showAndWait();
        if (choice.get() == buttonTypeOne){
            employeeDetailsPane();
        }
        if (choice.get() == buttonTypeTwo) {
            System.exit(0);
        }

    }

    /* delete Alert */
    private void deleteAlert(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Deletion Success Message");
        alert.setHeaderText("Employee has been deleted");
        alert.setContentText("Choose your option.");
        ButtonType buttonTypeOne = new ButtonType("Show Employees");
        ButtonType buttonTypeTwo = new ButtonType("Exit");

        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo);

        Optional<ButtonType> choice = alert.showAndWait();
        if (choice.get() == buttonTypeOne){
            employeeDetailsPane();
        }
        if (choice.get() == buttonTypeTwo) {
            System.exit(0);
        }

    }

    /* nonValidEmail */
    private void nonValidEmailAlert(){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning Dialog");
        alert.setHeaderText("Non Valide Email");
        alert.setContentText("Enter Valid email in form asd@asd.com");
        alert.showAndWait();
    }

    /* emptyInput */

    private void emptyInputAlert(){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning Dialog");
        alert.setHeaderText("Empty Input field");
        alert.setContentText("Fill All Inputs");
        alert.showAndWait();
    }


}
