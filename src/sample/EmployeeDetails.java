package sample;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public  class EmployeeDetails extends AnchorPane {

    protected final GridPane EmployeeDetailsGridPane;
    protected final ColumnConstraints columnConstraints;
    protected final ColumnConstraints columnConstraints0;
    protected final ColumnConstraints columnConstraints1;
    protected final ColumnConstraints columnConstraints2;
    protected final RowConstraints rowConstraints;
    protected final Button previousButton;
    protected final Button nextButton;
    protected final Button deleteButton;
    protected final Button editButton;
    protected final Label nameLabel;
    protected final Label phoneLabel;
    protected final Label addressLabel;
    protected final Label emailLabel;
    protected final Text nameTextArea;
    protected final Text phoneTextArea;
    protected final Text addressTextArea;
    protected final Text emailTextArea;

    public EmployeeDetails() {

        EmployeeDetailsGridPane = new GridPane();
        columnConstraints = new ColumnConstraints();
        columnConstraints0 = new ColumnConstraints();
        columnConstraints1 = new ColumnConstraints();
        columnConstraints2 = new ColumnConstraints();
        rowConstraints = new RowConstraints();
        previousButton = new Button();
        nextButton = new Button();
        deleteButton = new Button();
        editButton = new Button();
        nameLabel = new Label();
        phoneLabel = new Label();
        addressLabel = new Label();
        emailLabel = new Label();
        nameTextArea = new Text();
        phoneTextArea = new Text();
        addressTextArea = new Text();
        emailTextArea = new Text();

        setMaxHeight(USE_PREF_SIZE);
        setMaxWidth(USE_PREF_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefHeight(400.0);
        setPrefWidth(600.0);

        EmployeeDetailsGridPane.setLayoutY(327.0);
        EmployeeDetailsGridPane.setPrefHeight(73.0);
        EmployeeDetailsGridPane.setPrefWidth(600.0);

        columnConstraints.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints.setMinWidth(10.0);
        columnConstraints.setPrefWidth(100.0);

        columnConstraints0.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints0.setMinWidth(10.0);
        columnConstraints0.setPrefWidth(100.0);

        columnConstraints1.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints1.setMinWidth(10.0);
        columnConstraints1.setPrefWidth(100.0);

        columnConstraints2.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints2.setMinWidth(10.0);
        columnConstraints2.setPrefWidth(100.0);

        rowConstraints.setMinHeight(10.0);
        rowConstraints.setPrefHeight(30.0);
        rowConstraints.setVgrow(javafx.scene.layout.Priority.SOMETIMES);

        previousButton.setId("previousButton");
        previousButton.setMnemonicParsing(false);
        previousButton.setPrefHeight(98.0);
        previousButton.setPrefWidth(315.0);
        previousButton.setText("Previous");

        GridPane.setColumnIndex(nextButton, 1);
        nextButton.setId("nextButton");
        nextButton.setMnemonicParsing(false);
        nextButton.setPrefHeight(159.0);
        nextButton.setPrefWidth(341.0);
        nextButton.setText("Next");

        GridPane.setColumnIndex(deleteButton, 3);
        deleteButton.setId("deleteButton");
        deleteButton.setMnemonicParsing(false);
        deleteButton.setPrefHeight(197.0);
        deleteButton.setPrefWidth(269.0);
        deleteButton.setText("Delete");

        GridPane.setColumnIndex(editButton, 2);
        editButton.setId("editButton");
        editButton.setMnemonicParsing(false);
        editButton.setPrefHeight(189.0);
        editButton.setPrefWidth(253.0);
        editButton.setText("Edit");

        nameLabel.setId("nameLabel");
        nameLabel.setLayoutX(31.0);
        nameLabel.setLayoutY(14.0);
        nameLabel.setPrefHeight(57.0);
        nameLabel.setPrefWidth(95.0);
        nameLabel.setText("Name:");
        nameLabel.setFont(new Font("System Bold", 24.0));

        phoneLabel.setId("phoneLabel");
        phoneLabel.setLayoutX(31.0);
        phoneLabel.setLayoutY(78.0);
        phoneLabel.setPrefHeight(57.0);
        phoneLabel.setPrefWidth(95.0);
        phoneLabel.setText("Phone:");
        phoneLabel.setFont(new Font("System Bold", 24.0));

        addressLabel.setId("addressLabel");
        addressLabel.setLayoutX(31.0);
        addressLabel.setLayoutY(143.0);
        addressLabel.setPrefHeight(57.0);
        addressLabel.setPrefWidth(122.0);
        addressLabel.setText("Address:");
        addressLabel.setFont(new Font("System Bold", 24.0));

        emailLabel.setId("emailLabel");
        emailLabel.setLayoutX(31.0);
        emailLabel.setLayoutY(216.0);
        emailLabel.setPrefHeight(57.0);
        emailLabel.setPrefWidth(95.0);
        emailLabel.setText("Email:");
        emailLabel.setFont(new Font("System Bold", 24.0));

        nameTextArea.setId("nameTextArea");
        nameTextArea.setLayoutX(240.0);
        nameTextArea.setLayoutY(50.0);
        nameTextArea.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        nameTextArea.setStrokeWidth(0.0);
        nameTextArea.setWrappingWidth(271.7294921875);
        nameTextArea.setFont(new Font(24.0));

        phoneTextArea.setId("phoneTextArea");
        phoneTextArea.setLayoutX(240.0);
        phoneTextArea.setLayoutY(114.0);
        phoneTextArea.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        phoneTextArea.setStrokeWidth(0.0);
        phoneTextArea.setWrappingWidth(271.7294921875);
        phoneTextArea.setFont(new Font(24.0));

        addressTextArea.setId("addressTextArea");
        addressTextArea.setLayoutX(240.0);
        addressTextArea.setLayoutY(179.0);
        addressTextArea.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        addressTextArea.setStrokeWidth(0.0);
        addressTextArea.setWrappingWidth(271.7294921875);
        addressTextArea.setFont(new Font(24.0));

        emailTextArea.setId("emailTextArea");
        emailTextArea.setLayoutX(240.0);
        emailTextArea.setLayoutY(252.0);
        emailTextArea.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        emailTextArea.setStrokeWidth(0.0);
        emailTextArea.setWrappingWidth(271.7294921875);
        emailTextArea.setFont(new Font(18.0));

        EmployeeDetailsGridPane.getColumnConstraints().add(columnConstraints);
        EmployeeDetailsGridPane.getColumnConstraints().add(columnConstraints0);
        EmployeeDetailsGridPane.getColumnConstraints().add(columnConstraints1);
        EmployeeDetailsGridPane.getColumnConstraints().add(columnConstraints2);
        EmployeeDetailsGridPane.getRowConstraints().add(rowConstraints);
        EmployeeDetailsGridPane.getChildren().add(previousButton);
        EmployeeDetailsGridPane.getChildren().add(nextButton);
        EmployeeDetailsGridPane.getChildren().add(deleteButton);
        EmployeeDetailsGridPane.getChildren().add(editButton);
        getChildren().add(EmployeeDetailsGridPane);
        getChildren().add(nameLabel);
        getChildren().add(phoneLabel);
        getChildren().add(addressLabel);
        getChildren().add(emailLabel);
        getChildren().add(nameTextArea);
        getChildren().add(phoneTextArea);
        getChildren().add(addressTextArea);
        getChildren().add(emailTextArea);

    }
}
