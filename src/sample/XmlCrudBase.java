package sample;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

public  class XmlCrudBase extends BorderPane {

    protected final MenuBar menuBar;
    protected final Menu menu;
    protected final MenuItem openMenuItem;
    protected final SeparatorMenuItem openShowSeparator;
    protected final MenuItem showMenuItem;
    protected final SeparatorMenuItem separatorMenuItem;
    protected final MenuItem newMenuItem;
    protected final SeparatorMenuItem separatorMenuItem0;
    protected final MenuItem editMenuItem;
    protected final SeparatorMenuItem separatorMenuItem1;
    protected final MenuItem closeMenuItem;
    protected final Menu menu0;
    protected final MenuItem aboutMenuItem;
    protected final Menu menu1;
    protected final MenuItem xmlCrudHelpMenuItem;
    protected final AnchorPane anchorPane;

    public XmlCrudBase() {

        menuBar = new MenuBar();
        menu = new Menu();
        openMenuItem = new MenuItem();
        openShowSeparator = new SeparatorMenuItem();
        showMenuItem = new MenuItem();
        separatorMenuItem = new SeparatorMenuItem();
        newMenuItem = new MenuItem();
        separatorMenuItem0 = new SeparatorMenuItem();
        editMenuItem = new MenuItem();
        separatorMenuItem1 = new SeparatorMenuItem();
        closeMenuItem = new MenuItem();
        menu0 = new Menu();
        aboutMenuItem = new MenuItem();
        menu1 = new Menu();
        xmlCrudHelpMenuItem = new MenuItem();
        anchorPane = new AnchorPane();

        setMaxHeight(USE_PREF_SIZE);
        setMaxWidth(USE_PREF_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefHeight(400.0);
        setPrefWidth(600.0);

        BorderPane.setAlignment(menuBar, javafx.geometry.Pos.CENTER);

        menu.setMnemonicParsing(false);
        menu.setText("File");

        openMenuItem.setId("openMenuItem");
        openMenuItem.setMnemonicParsing(false);
        openMenuItem.setText("Open");

        openShowSeparator.setId("openShowSeparator");
        openShowSeparator.setMnemonicParsing(false);
        openShowSeparator.setText("OpenShowSeparator");

        showMenuItem.setId("showMenuItem");
        showMenuItem.setMnemonicParsing(false);
        showMenuItem.setText("Show");

        separatorMenuItem.setId("NewOpenSeparator");
        separatorMenuItem.setMnemonicParsing(false);
        separatorMenuItem.setText("NewOpenSeparator");

        newMenuItem.setId("newMenuItem");
        newMenuItem.setMnemonicParsing(false);
        newMenuItem.setText("New");

        separatorMenuItem0.setId("openEditSeparator");
        separatorMenuItem0.setMnemonicParsing(false);
        separatorMenuItem0.setText("openEditSeparator");

        editMenuItem.setId("editMenuItem");
        editMenuItem.setMnemonicParsing(false);
        editMenuItem.setText("Edit");

        separatorMenuItem1.setId("editCloseSeparator");
        separatorMenuItem1.setMnemonicParsing(false);
        separatorMenuItem1.setText("EditCloseSeparator");

        closeMenuItem.setId("closeMenuItem");
        closeMenuItem.setMnemonicParsing(false);
        closeMenuItem.setText("Close");

        menu0.setId("aboutMenu");
        menu0.setMnemonicParsing(false);
        menu0.setText("About");

        aboutMenuItem.setId("aboutMenuItem");
        aboutMenuItem.setMnemonicParsing(false);
        aboutMenuItem.setText("About");

        menu1.setMnemonicParsing(false);
        menu1.setText("Help");

        xmlCrudHelpMenuItem.setId("xmlCrudHelpMenuItem");
        xmlCrudHelpMenuItem.setMnemonicParsing(false);
        xmlCrudHelpMenuItem.setText("XmlCrudHelp");
        setTop(menuBar);

        BorderPane.setAlignment(anchorPane, javafx.geometry.Pos.CENTER);
        anchorPane.setPrefHeight(200.0);
        anchorPane.setPrefWidth(200.0);
        setCenter(anchorPane);

        menu.getItems().add(openMenuItem);
        menu.getItems().add(openShowSeparator);
        menu.getItems().add(showMenuItem);
        menu.getItems().add(separatorMenuItem);
        menu.getItems().add(newMenuItem);
        menu.getItems().add(separatorMenuItem0);
        menu.getItems().add(editMenuItem);
        menu.getItems().add(separatorMenuItem1);
        menu.getItems().add(closeMenuItem);
        menuBar.getMenus().add(menu);
        menu0.getItems().add(aboutMenuItem);
        menuBar.getMenus().add(menu0);
        menu1.getItems().add(xmlCrudHelpMenuItem);
        menuBar.getMenus().add(menu1);

    }
}
